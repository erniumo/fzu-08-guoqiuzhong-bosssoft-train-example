package com.bosssoft.hr.train.j2se.basic.example.xml;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Student;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:13
 * @since
 **/
public class DOMOperation implements XMLOperation<Student> {
    @Override
    public boolean create(Student object) {
        try {
            String filepath = "src\\main\\resources\\student.tld";
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(filepath);
            Node root = doc.getFirstChild();
            NodeList nl = doc.getElementsByTagName("student");
            if(object != null){
                Element student = doc.createElement("student");
                student.setAttribute("id",object.getId().toString());
                Element name = doc.createElement("name");
                name.appendChild(doc.createTextNode(object.getName()));
                student.appendChild(name);
                Element age = doc.createElement("age");
                age.appendChild(doc.createTextNode(object.getAge().toString()));
                student.appendChild(age);

                root.appendChild(student);
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(filepath));
            transformer.transform(source, result);

            System.out.println("Done");

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        } catch (SAXException | IOException ioe) {
            ioe.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean remove(Student object) {
        try {
            String filepath = "src\\main\\resources\\student.tld";
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(filepath);
            Node root = doc.getFirstChild();
            NodeList nl = doc.getElementsByTagName("student");

            for (int i = 0; i < nl.getLength(); i++) {
                String id=nl.item(i).getAttributes().getNamedItem("id").getTextContent();
                if (id!=null&&object.getId().toString().equals(id)){
                    root.removeChild(nl.item(i));
                }
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(filepath));
            transformer.transform(source, result);

            System.out.println("Done");

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        } catch (SAXException | IOException ioe) {
            ioe.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean update(Student object) throws TransformerException {
        try {
            String filepath = "src\\main\\resources\\student.tld";
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(filepath);
            NodeList nl = doc.getElementsByTagName("student");

            for (int i = 0; i < nl.getLength(); i++) {
                String id=nl.item(i).getAttributes().getNamedItem("id").getTextContent();
                if (id!=null&&object.getId().toString().equals(id)){
                    Node stu = nl.item(i);
                    NamedNodeMap attr = stu.getAttributes();
                    Node nodeAttr = attr.getNamedItem("id");
                    nodeAttr.setTextContent(object.getId().toString());

                    NodeList list = stu.getChildNodes();
                    for (int j = 0; j < list.getLength(); j++) {
                        Node node = list.item(j);
                        if ("name".equals(node.getNodeName())) {
                            node.setTextContent(object.getName());
                        }
                        if ("age".equals(node.getNodeName())){
                            node.setTextContent(object.getAge().toString());
                        }
                    }
                }
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(filepath));
            transformer.transform(source, result);

            System.out.println("Done");

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        } catch (SAXException | IOException ioe) {
            ioe.printStackTrace();
        }
        return false;
    }

    @Override
    public Student query(Student object) {
        long lasting = System.currentTimeMillis();
        Student student = null;
        try {
            File f = new File("src\\main\\resources\\student.tld");
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(f);
            NodeList nl = doc.getElementsByTagName("student");
            for (int i = 0; i < nl.getLength(); i++) {
                String id=nl.item(i).getAttributes().getNamedItem("id").getTextContent();
                if (id!=null&&object.getId().toString().equals(id)){
                    student= new Student();
                    student.setId(Integer.parseInt(id));
                    student.setName(doc.getElementsByTagName("name").item(i).getFirstChild().getNodeValue());
                    student.setAge(Integer.parseInt(doc.getElementsByTagName("age").item(i).getFirstChild().
                            getNodeValue()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return student;
    }
}
