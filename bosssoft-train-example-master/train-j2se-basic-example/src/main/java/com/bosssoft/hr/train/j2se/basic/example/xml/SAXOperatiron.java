package com.bosssoft.hr.train.j2se.basic.example.xml;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Student;
import jdk.internal.org.xml.sax.Attributes;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Thread.*;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:10
 * @since
 **/
public class SAXOperatiron implements XMLOperation<Student>{
    @Override
    public boolean create(Student object) {
        return false;
    }

    @Override
    public boolean remove(Student object) {
        return false;
    }

    @Override
    public boolean update(Student object) {
        return false;
    }

    @Override
    public Student query(Student object) throws ParserConfigurationException, SAXException, IOException {
        // SAX解析
        // 1、获取解析工厂
        SAXParserFactory factory = SAXParserFactory.newInstance();
        // 2、从解析工厂获取解析器
        SAXParser parse = factory.newSAXParser();
        // 3、编写处理器
        // 4、加载文档 Document 注册处理器
        StudentHandler handler = new StudentHandler();
        // 5、解析
        String filepath = "src\\main\\resources\\student.tld";
        File file = new File(filepath);
        parse.parse(file,handler);
        List<Student> students = handler.getStudents();
        for (Student student : students) {
            System.out.println(student.getName() + "----" + student.getAge());
        }
        return null;
    }
}

class StudentHandler extends DefaultHandler {
    private List<Student> students;
    private Student student;
    private String tag;

    @Override
    public void startDocument() {
        System.out.println("startDocument-------");
        students = new ArrayList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, org.xml.sax.Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        System.out.println("startElement-------" + qName);
        if (null != qName) {
            tag = qName;
            if (tag.equals("student")) {
                student = new Student();
            }
        }
    }

   /* @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {

    }*/

    @Override
    public void characters(char[] ch, int start, int length) {
        String contents = new String(ch, start, length);
        System.out.println("characters--------" + contents);
        if (tag != null) {
            if (tag.equals("name")) {
                student.setName(contents);
            } else if (tag.equals("age")) {
                if (contents.length() > 0) {
                    student.setAge(Integer.valueOf(contents));
                }
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        System.out.println("endElement-------" + qName);
        if (null != qName) {
            if (qName.equals("student")) {
                students.add(student);
            }
        }
        tag = null;
    }

    @Override
    public void endDocument() {
        System.out.println("endDocument-------");
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

}
