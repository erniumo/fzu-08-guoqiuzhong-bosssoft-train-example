package com.bosssoft.hr.train.j2se.basic.example.database;

import com.sun.rowset.CachedRowSetImpl;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.logging.Logger;


/**
 * @description: 我是工具类并且我不喜欢被继承 final 保护了我免于继承，private 保护我被创建
 * @author: Administrator
 * @create: 2020-05-28 20:45
 * @since
 **/
@Slf4j
public final class DBUtil {
    private static final String URL = "jdbc:mysql://localhost/learn?serverTimezone=Asia/Shanghai";
    private static final String USER = "root";
    private static final String PASSWORD = "123456";
    private static Connection conn = null;
    private static ResultSet rs = null;
    private static PreparedStatement ps = null;

    private DBUtil() {

    }


    public static Connection init() {
        try {
            Class.forName("com.mysql.jc.jdbc.Driver");
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            return conn;
        } catch (Exception e) {
            log.error("DBUtil.init：获取数据库连接失败！");
        }
        return null;
    }

    public static int creatOrUpdateOrDelete(String sql) {
        int result = -1;
        if (conn != null) {
            try {
                ps = conn.prepareStatement(sql);
                //执行
                result = ps.executeUpdate();
            } catch (SQLException e) {
                log.error("数据库修改异常");
            }
        } else {
            log.error("DBUtil.CreatOrUpdateOrDelete：获取数据库连接失败！");
        }
        return result;
    }


    public static int creatOrUpdateOrDelete(String sql, String... columns) {
        int result = -1;
        if (conn != null) {
            try {
                ps = conn.prepareStatement(sql);
                for (int i = 1; i <= columns.length; i++) {
                    ps.setString(i, columns[i - 1]);
                }
                //执行
                result = ps.executeUpdate();
            } catch (SQLException e) {
                log.error("数据库修改异常");
            }
        } else {
            log.error("DBUtil.CreatOrUpdateOrDelete：获取数据库连接失败！");
        }
        return result;
    }


    public static ResultSet select(String sql) {
        try {
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery(sql);
        } catch (SQLException e) {
            log.error("sql数据库查询异常");

        }
        return rs;
    }

    public static void closeConn() {
        try {
            conn.close();
        } catch (SQLException e) {
            log.error("sql数据库关闭异常");

        }
    }
}
