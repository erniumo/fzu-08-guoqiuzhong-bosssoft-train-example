package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author lenovo
 */
public class QueueExampleImpl implements QueueExmaple{
    Queue<User> queue = new LinkedList<User>();

    public Boolean offer(User node){ return queue.offer(node);}

    public User poll(){ return queue.poll(); }

    public User element(){ return queue.element(); }

}
