package com.bosssoft.hr.train.j2se.basic.example.socket;

import java.io.IOException;
import java.nio.channels.SelectionKey;

public interface ServerHandlerBs {
    void handleAccept(SelectionKey selectionKey) throws IOException;

    String handleRead(SelectionKey selectionKey) throws IOException;
}
