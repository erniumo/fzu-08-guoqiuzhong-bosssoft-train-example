package com.bosssoft.hr.train.j2se.basic.example.thread;

import jdk.internal.instrumentation.Logger;

import java.util.Random;
import java.util.concurrent.*;

/**
 * @author lenovo
 */
public class ThreadPoolexample {

    public ThreadPoolExecutor testThreadPool() {
        // 指定核心线程数量、最多线程数量、存活时间、阻塞队列、拒绝策略
        return new ThreadPoolExecutor(5,
                10,
                60,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(10),
                new ThreadPoolExecutor.DiscardPolicy());

    }
}

class Task implements Runnable {
    //标识
    private int index;

    public Task(int index) {
        this.index = index;
    }

    @Override
    public void run() {
        int runTime = new Random().nextInt(5) + 1;
        Logger log = null;
        log.info("正在执行：{"+ index +"},运行时间为：{"+ runTime +"} ");
        try {
            Thread.sleep((long) runTime * 100);
            log.info("线程{"+ index +"}执行完毕");
        } catch (Exception e) {
            log.warn("线程睡眠被打断");
        }

    }
}