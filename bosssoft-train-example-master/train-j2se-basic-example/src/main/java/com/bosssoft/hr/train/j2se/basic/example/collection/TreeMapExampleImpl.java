package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Resource;
import com.bosssoft.hr.train.j2se.basic.example.pojo.Role;

import java.util.TreeMap;

/**
 * @author lenovo
 */
public class TreeMapExampleImpl {
    TreeMap<Role, Resource> pairs = new TreeMap<>();

    public Resource put(Role role,Resource resource){ return pairs.put(role,resource);}

    public Boolean containsKey(Role role){ return pairs.containsKey(role);}

    public Boolean containsValue(Resource resource){ return pairs.containsValue(resource);}

    public Resource remove(Role role){ return pairs.remove(role); }
}
