package com.bosssoft.hr.train.j2se.basic.example.annotation;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:42
 * @since
 **/
@Table(value = "user")
public class UserModel extends BaseModel
{
    @Id(value = "id")
    private int id;

    @Column(value = "name")
    private String name;

    @Column(value = "age")
    private int age;

    public UserModel(Integer Id, String name, Integer age) {
        this.id = Id;
        this.name = name;
        this.age = age;
    }
    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "Id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

}
