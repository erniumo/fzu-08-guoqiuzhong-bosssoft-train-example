package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Resource;
import com.bosssoft.hr.train.j2se.basic.example.pojo.Role;
import org.junit.Test;


public class TreeMapExampleImplTest {
    TreeMapExampleImpl treeMapExample = new TreeMapExampleImpl();
    @Test
    public void put() {
        Role role = new Role();
        Resource resource = new Resource();
        role.setId(1);
        role.setName("一");
        resource.setName("一");
        resource.setId(1);
        System.out.println(treeMapExample.put(role,resource));
    }

    @Test
    public void containsKey() {
        Role role = new Role();
        Resource resource = new Resource();
        role.setId(1);
        role.setName("一");
        resource.setName("一");
        resource.setId(1);
        System.out.println(treeMapExample.put(role,resource));
        System.out.println(treeMapExample.containsKey(role));
    }

    @Test
    public void containsValue() {
        Role role = new Role();
        Resource resource = new Resource();
        role.setId(1);
        role.setName("一");
        resource.setName("一");
        resource.setId(1);
        System.out.println(treeMapExample.put(role,resource));
        System.out.println(treeMapExample.containsValue(resource));
    }

    @Test
    public void remove(){
        Role role = new Role();
        Resource resource = new Resource();
        role.setId(1);
        role.setName("一");
        resource.setName("一");
        resource.setId(1);
        System.out.println(treeMapExample.put(role,resource));
        System.out.println(treeMapExample.remove(role));
    }
}
