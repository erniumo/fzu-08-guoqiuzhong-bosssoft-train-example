package com.bosssoft.hr.train.j2se.basic.example.thread;


import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.ThreadPoolExecutor;

import static org.junit.Assert.*;


public class ThreadPoolexampleTest {
    ThreadPoolexample threadPoolexample = new ThreadPoolexample();
    @Test
    public void threadPool() {
        ThreadPoolExecutor executor = threadPoolexample.testThreadPool();
        for(int i=0;i<20;i++){
            Task task=new Task(i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            executor.execute(task);
        }
    }
}
