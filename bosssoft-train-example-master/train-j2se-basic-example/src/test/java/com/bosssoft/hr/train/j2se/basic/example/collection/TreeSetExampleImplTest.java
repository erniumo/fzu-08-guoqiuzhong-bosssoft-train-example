package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import org.junit.Test;

import static org.junit.Assert.*;

public class TreeSetExampleImplTest {

    TreeSetExampleImpl treeSetExample = new TreeSetExampleImpl();
    @Test
    public void sort() {
        User[] users = new User[3];
        users[0] = new User();
        users[1] = new User();
        users[2] = new User();
        users[0].setId(1);
        users[0].setName("张三");
        users[1].setId(3);
        users[1].setName("李四");
        users[2].setId(2);
        users[2].setName("王五");

        User[] users1 = treeSetExample.sort(users);
        System.out.println(users1[2].getId());
    }
}