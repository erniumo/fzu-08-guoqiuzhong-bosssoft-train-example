package com.bosssoft.hr.train.j2se.basic.example.xml;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Student;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.xml.transform.TransformerException;

import static org.junit.Assert.*;

public class DOMOperationTest {

    DOMOperation domOperation = new DOMOperation();
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void create() {
        Student student = new Student();
        student.setId(1);
        student.setName("张四");
        student.setAge(11);
        domOperation.create(student);
    }

    @Test
    public void remove() {
        Student student = new Student();
        student.setId(1);
        student.setName("张四");
        student.setAge(11);
        domOperation.remove(student);
    }

    @Test
    public void update() throws TransformerException {
        Student student = new Student();
        student.setId(1);
        student.setName("张四");
        student.setAge(11);
        domOperation.update(student);
    }

    @Test
    public void query() {
        Student student = new Student();
        student.setId(1);
        student.setName("张三");
        student.setAge(10);
        Student student1 = domOperation.query(student);
        if (student1!=null)
        System.out.println(student1.getAge());

    }
}