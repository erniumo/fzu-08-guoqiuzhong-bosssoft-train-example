package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

public class QueueExampleImplTest {

    QueueExampleImpl queueExample = new QueueExampleImpl();
    @Test
    public void offer() {
        User user = new User();
        user.setId(1);
        user.setName("张三");
        User user1 = new User();
        user1.setId(2);
        user1.setName("李四");
        queueExample.offer(user);
        queueExample.offer(user1);
    }

    @Test
    public void poll() {
        User user = new User();
        user.setId(1);
        user.setName("张三");
        User user1 = new User();
        user1.setId(2);
        user1.setName("李四");
        queueExample.offer(user);
        queueExample.offer(user1);
        System.out.println(queueExample.poll().getId());
    }

    @Test
    public void element() {
        User user = new User();
        user.setId(1);
        user.setName("张三");
        User user1 = new User();
        user1.setId(2);
        user1.setName("李四");
        queueExample.offer(user);
        queueExample.offer(user1);
        System.out.println(queueExample.element().getId());
    }
}
