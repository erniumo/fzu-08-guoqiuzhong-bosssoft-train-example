package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Resource;
import com.bosssoft.hr.train.j2se.basic.example.pojo.Role;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class HashMapExampleImplTest {

    HashMapExampleImpl hashMapExample = new HashMapExampleImpl();
    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void put() {
        Role role = new Role();
        Resource resource = new Resource();
        role.setId(1);
        role.setName("一");
        resource.setName("一");
        resource.setId(1);
        System.out.println(hashMapExample.put(role,resource));
    }

    @Test
    public void remove() {
        Role role = new Role();
        Resource resource = new Resource();
        role.setId(1);
        role.setName("一");
        resource.setName("一");
        resource.setId(1);
        System.out.println(hashMapExample.put(role,resource));
        System.out.println(hashMapExample.remove(role));
    }

    @Test
    public void containsKey() {
        Role role = new Role();
        Resource resource = new Resource();
        role.setId(1);
        role.setName("一");
        resource.setName("一");
        resource.setId(1);
        System.out.println(hashMapExample.put(role,resource));
        System.out.println(hashMapExample.containsKey(role));
    }

    @Test
    public void visitByEntryset() {
        Role role = new Role();
        Resource resource = new Resource();
        role.setId(1);
        role.setName("一");
        resource.setName("一");
        resource.setId(1);
        System.out.println(hashMapExample.put(role,resource));
        hashMapExample.visitByEntryset();
    }

    @Test
    public void visitByKeyset() {
        Role role = new Role();
        Resource resource = new Resource();
        role.setId(1);
        role.setName("一");
        resource.setName("一");
        resource.setId(1);
        System.out.println(hashMapExample.put(role,resource));
        hashMapExample.visitByKeyset();
    }

    @Test
    public void visitByValues() {
        Role role = new Role();
        Resource resource = new Resource();
        role.setId(1);
        role.setName("一");
        resource.setName("一");
        resource.setId(1);
        System.out.println(hashMapExample.put(role,resource));
        hashMapExample.visitByValues();
    }
}