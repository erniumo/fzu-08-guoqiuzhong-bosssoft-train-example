package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Resource;
import com.bosssoft.hr.train.j2se.basic.example.pojo.Role;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;


public class ConcurrentHashMapExampleImplTest {
    ConcurrentHashMapExampleImpl concurrentHashMapExample = new ConcurrentHashMapExampleImpl();
    @Test
    public void put() {
        Role role = new Role();
        Resource resource = new Resource();
        role.setId(1);
        role.setName("一");
        resource.setName("一");
        resource.setId(1);
        System.out.println(concurrentHashMapExample.put(role,resource));
    }

    @Test
    public void remove() {
        Role role = new Role();
        Resource resource = new Resource();
        role.setId(1);
        role.setName("一");
        resource.setName("一");
        resource.setId(1);
        System.out.println(concurrentHashMapExample.put(role,resource));
        System.out.println(concurrentHashMapExample.remove(role));
    }

    @Test
    public void containsKey() {
        Role role = new Role();
        Resource resource = new Resource();
        role.setId(1);
        role.setName("一");
        resource.setName("一");
        resource.setId(1);
        System.out.println(concurrentHashMapExample.put(role,resource));
        System.out.println(concurrentHashMapExample.containsKey(role));
    }

    @Test
    public void visitByEntryset() {
        Role role = new Role();
        Resource resource = new Resource();
        role.setId(1);
        role.setName("一");
        resource.setName("一");
        resource.setId(1);
        System.out.println(concurrentHashMapExample.put(role,resource));
        concurrentHashMapExample.visitByEntryset();
    }

    @Test
    public void visitByEntryset_Lambda() {
        Role role = new Role();
        Resource resource = new Resource();
        role.setId(1);
        role.setName("一");
        resource.setName("一");
        resource.setId(1);
        System.out.println(concurrentHashMapExample.put(role,resource));
        concurrentHashMapExample.visitByEntryset_Lambda();
    }

    @Test
    public void visitByKeyset() {
        Role role = new Role();
        Resource resource = new Resource();
        role.setId(1);
        role.setName("一");
        resource.setName("一");
        resource.setId(1);
        System.out.println(concurrentHashMapExample.put(role,resource));
        concurrentHashMapExample.visitByKeyset();
    }

    @Test
    public void visitByValues() {
        Role role = new Role();
        Resource resource = new Resource();
        role.setId(1);
        role.setName("一");
        resource.setName("一");
        resource.setId(1);
        System.out.println(concurrentHashMapExample.put(role,resource));
        concurrentHashMapExample.visitByValues();
    }
}
