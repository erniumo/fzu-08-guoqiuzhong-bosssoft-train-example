package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedListExampleImplTest {

    LinkedListExampleImpl linkedListExample = new LinkedListExampleImpl();
    @Before
    public void setUp() throws Exception {
        User user = new User();
        user.setId(1);
        user.setName("张三");
        linkedListExample.append(user);
        User user1 = new User();
        user1.setId(2);
        user1.setName("李四");
        linkedListExample.append(user1);
    }

    @After
    public void tearDown() throws Exception {
        linkedListExample.remove(0);
    }

    @Test
    public void append() {
        User user = new User();
        user.setId(1);
        user.setName("张三");
        linkedListExample.append(user);
    }

    @Test
    public void get() {
        User user = linkedListExample.get(0);
        System.out.println("id:" + user.getId()+"name:"+user.getName());
    }

    @Test
    public void insert() {
        User user = new User();
        user.setId(3);
        user.setName("王五");
        linkedListExample.insert(1,user);
        User user1 = linkedListExample.get(1);
        System.out.println("id:" + user1.getId()+"name:"+user1.getName());

    }

    @Test
    public void remove() {
        linkedListExample.remove(0);
        User user = linkedListExample.get(0);
        System.out.println("id:" + user.getId()+"name:"+user.getName());
    }

    @Test
    public void listByIndex() {
        linkedListExample.listByIndex();
    }

    @Test
    public void listByIterator() {
        linkedListExample.listByIterator();
    }

    @Test
    public void toArray() {
        User[] users = linkedListExample.toArray();
        for (int i=0;i<users.length;i++){
            System.out.println("id"+users[i].getId()+"name:"+users[i].getName());
        }
    }

    @Test
    public void sort() {
        linkedListExample.sort();
        User[] users = linkedListExample.toArray();
        for (int i=0;i<users.length;i++){
            System.out.println("id"+users[i].getId()+"name:"+users[i].getName());
        }
    }

    @Test
    public void sort2() {
        linkedListExample.sort2();
        User[] users = linkedListExample.toArray();
        for (int i=0;i<users.length;i++){
            System.out.println("id"+users[i].getId()+"name:"+users[i].getName());
        }
    }

    @Test
    public void addFirst() {
        User user = new User();
        user.setId(3);
        user.setName("王五");
        linkedListExample.addFirst(user);
        System.out.println("id"+linkedListExample.get(0).getId());
    }

    @Test
    public void offer() {
        User user = new User();
        user.setId(3);
        user.setName("王五");
        linkedListExample.offer(user);
        System.out.println("id"+linkedListExample.get(2).getId());
    }

    @Test
    public void sychronizedVisit() {
        User user = new User();
        user.setId(3);
        user.setName("王五");
        linkedListExample.sychronizedVisit(user);
        System.out.println("id"+linkedListExample.get(0).getId());
    }

    @Test
    public void push() {
        User user = new User();
        user.setId(3);
        user.setName("王五");
        linkedListExample.push(user);
        System.out.println("id"+linkedListExample.get(0).getId());
    }

    @Test
    public void pop() {
        User user = linkedListExample.pop();
        System.out.println("id"+user.getId());
    }
}