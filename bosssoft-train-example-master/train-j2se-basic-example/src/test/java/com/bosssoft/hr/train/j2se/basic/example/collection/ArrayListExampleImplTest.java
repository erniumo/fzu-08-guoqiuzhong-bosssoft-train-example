package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class ArrayListExampleImplTest {

    ArrayListExampleImpl arrayListExample = new ArrayListExampleImpl();
    @Before
    public void setUp() throws Exception {
        User user = new User();
        user.setId(1);
        user.setName("张三");
        arrayListExample.append(user);
        User user1 = new User();
        user1.setId(2);
        user1.setName("李四");
        arrayListExample.append(user1);
    }

    @After
    public void tearDown() throws Exception {
        arrayListExample.remove(0);
    }

    @Test
    public void append() {
        User user = new User();
        user.setId(2);
        user.setName("李四");
        arrayListExample.append(user);
    }

    @Test
    public void get() {
        User user1 = arrayListExample.get(0);
        System.out.println("id:"+ user1.getId() + "name:" + user1.getName());
    }

    @Test
    public void insert() {
        User user = new User();
        user.setId(2);
        user.setName("李四");
        arrayListExample.insert(0,user);
        User user1 = arrayListExample.get(1);
        System.out.println("id:"+ user1.getId() + "name:" + user1.getName());
    }

    @Test
    public void remove() {
        arrayListExample.remove(0);
        if(arrayListExample.get(0)==null)System.out.println("OK");
    }

    @Test
    public void listByIndex() {
        arrayListExample.listByIndex();
    }

    @Test
    public void listByIterator() {
        arrayListExample.listByIterator();
    }

    @Test
    public void toArray() {
        User[] users = arrayListExample.toArray();
        for(int i=0;i<users.length;i++){
            System.out.println("id:"+ users[i].getId() + "name:" + users[i].getName());
        }
    }

    @Test
    public void sort() {
        arrayListExample.sort();
        User[] users = arrayListExample.toArray();
        for(int i=0;i<users.length;i++){
            System.out.println("id:"+ users[i].getId() + "name:" + users[i].getName());
        }
    }

    @Test
    public void sort2() {
        arrayListExample.sort();
        User[] users = arrayListExample.toArray();
        for(int i=0;i<users.length;i++){
            System.out.println("id:"+ users[i].getId() + "name:" + users[i].getName());
        }
    }
}