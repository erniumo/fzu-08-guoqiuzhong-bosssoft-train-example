package com.bosssoft.hr.train.jsp.example.pojo;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-29 14:09
 * @since
 **/
public class Query {
    private Integer id;

    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
}
