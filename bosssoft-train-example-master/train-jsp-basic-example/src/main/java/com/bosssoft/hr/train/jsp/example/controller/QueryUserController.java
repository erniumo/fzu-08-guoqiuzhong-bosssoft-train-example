package com.bosssoft.hr.train.jsp.example.controller;

import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.service.UserService;
import com.bosssoft.hr.train.jsp.example.service.impl.UserServiceImpl;
import lombok.SneakyThrows;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 11:22
 * @since
 **/
public class QueryUserController extends HttpServlet {
    private UserService userService=new UserServiceImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String flag = req.getParameter("flag");
        if("1".equals(flag)){
            Query query = new Query();
            query.setName(req.getParameter("name"));
            List<User> users =(List)userService.queryByCondition(query);
            req.setAttribute("user",users.get(0));
            req.getRequestDispatcher("/result.jsp").forward(req,resp);
        }
        else {
            //校验参数合法性如果没问题才调用
            Query query = new Query();
            List<User> users =(List)userService.queryByCondition(query);
            req.setAttribute("users",users);
            req.getRequestDispatcher("/system.jsp").forward(req,resp);
        }
    }
}
