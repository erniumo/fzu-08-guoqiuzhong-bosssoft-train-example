package com.bosssoft.hr.train.jsp.example.service.impl;

import com.bosssoft.hr.train.jsp.example.dao.UserDao;
import com.bosssoft.hr.train.jsp.example.dao.daoImpl.UserDaoImpl;
import com.bosssoft.hr.train.jsp.example.exception.BusinessException;
import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.service.UserService;
import com.bosssoft.hr.train.jsp.example.util.DBUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 10:24
 * @since
 **/

public class UserServiceImpl implements UserService {
    UserDao userDao = new UserDaoImpl();
    @Override
    public boolean save(User user) {
        try {
            // do you logic
            userDao.insert(user);
            return true;
        }catch (Exception ex){
            throw new BusinessException("10001",ex.getMessage(),ex);
        }

    }

    @Override
    public boolean remove(User user) throws SQLException {
        userDao.deleteById(user.getId());
        return false;
    }

    @Override
    public boolean update(User user) throws SQLException {

        userDao.update(user);
        return false;
    }

    @Override
    public List<User> queryByCondition(Query queryCondition) throws SQLException {
        return userDao.queryByCondition(queryCondition);
    }

    @Override
    public boolean authentication(User user) throws SQLException {
        String code = user.getCode();
        Query query = new Query();
        query.setCode(code);
        List<User> users = userDao.queryByCondition(query);
        if (users.size()==0){
            return  false;
        }
        String password = users.get(0).getPassword();
        if (password.equals(user.getPassword())){
            return true;
        }
        return false;
    }
}
