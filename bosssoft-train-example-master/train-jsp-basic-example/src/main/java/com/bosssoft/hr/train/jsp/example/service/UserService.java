package com.bosssoft.hr.train.jsp.example.service;

import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;

import java.sql.SQLException;
import java.util.List;

/**
 * @description: 用户服务的接口
 * @author: Administrator
 * @create: 2020-05-30 10:17
 * @since
 **/
public interface UserService {
    boolean save(User user);
    boolean remove(User user) throws SQLException;
    boolean update(User user) throws SQLException;
    List<User> queryByCondition(Query queryCondition) throws SQLException;

    /**
     *  给 LoginController 调用
     * @param user
     * @return
     */
    boolean authentication(User user) throws SQLException;

}
