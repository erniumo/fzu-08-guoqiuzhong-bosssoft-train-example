package com.bosssoft.hr.train.jsp.example.controller;

import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.service.UserService;
import com.bosssoft.hr.train.jsp.example.service.impl.UserServiceImpl;
import lombok.SneakyThrows;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 10:11
 * @since
 **/
public class LoginController extends HttpServlet {
    UserService userService = new UserServiceImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
        doPost(req,resp);
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String F = req.getParameter("flag");
        if(F==null){
            String username = req.getParameter("username");
            String password = req.getParameter("password");
            boolean flag = authentication(username, password);
            if (flag){
                resp.getWriter().write("{\"code\":\"0\"}");
                System.out.println("OK");
//            req.getRequestDispatcher("system.jsp").forward(req,resp);

            }
            else {
                System.out.println("NO");
                resp.getWriter().write("{\"code\":\"1\"}");
                //req.getRequestDispatcher("error.jsp").forward(req,resp);
            }
        }else {
            req.getRequestDispatcher("/findUsers?flag=0").forward(req,resp);
        }

    }

    /**
     *  public 后面改为 private
     * @param code
     * @param password
     * @return
     */
    private boolean authentication(String code,String password) throws SQLException {
        User user = new User(code,password);
        if(userService.authentication(user)){
            return true;
        }
        return false;
    }
}
