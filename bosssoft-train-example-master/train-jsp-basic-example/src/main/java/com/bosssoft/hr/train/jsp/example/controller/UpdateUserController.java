package com.bosssoft.hr.train.jsp.example.controller;

import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.service.UserService;
import com.bosssoft.hr.train.jsp.example.service.impl.UserServiceImpl;
import com.mysql.cj.Session;
import lombok.SneakyThrows;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 11:22
 * @since
 **/
public class UpdateUserController extends HttpServlet {
    private UserService userService=new UserServiceImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String flag = req.getParameter("flag1");
        if("0".equals(flag)){
            Integer id =Integer.parseInt(req.getParameter("id"));
            Query query = new Query();
            query.setId(id);
            List<User> user = userService.queryByCondition(query);
            req.setAttribute("user",user.get(0));
            req.getRequestDispatcher("/update.jsp").forward(req,resp);
        }else {
            User user = new User();
            user.setId(Integer.parseInt(req.getParameter("id")));
            user.setName(req.getParameter("username"));
            user.setCode(req.getParameter("code"));
            user.setPassword(req.getParameter("password"));
            userService.update(user);
            req.setAttribute("flag","0");
            req.getRequestDispatcher("/findUsers").forward(req,resp);
        }

    }

}
