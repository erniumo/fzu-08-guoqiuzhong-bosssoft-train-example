package com.bosssoft.hr.train.jsp.example.dao.daoImpl;

import com.bosssoft.hr.train.jsp.example.dao.UserDao;
import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.util.DBUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 10:42
 * @since
 **/
public class UserDaoImpl implements UserDao {
    @Override
    public int insert(User user) throws SQLException {
        String sql = "insert into t_user (id,name,code,password) values ("+ user.getId() + ",'" + user.getName() +
                "' , '" + user.getCode() + " ', '" + user.getPassword() + "' ) ";
        DBUtil.executeUpdate(sql);
        return 0;
    }

    @Override
    public int deleteById(Integer id) throws SQLException {
        String sql = "delete from t_user where id="+ id;
        DBUtil.executeUpdate(sql);
        return 0;
    }

    @Override
    public int update(User user) throws SQLException {
        String sql = "update t_user  set name= '"+ user.getName() + "', code= '"+ user.getCode() +"',password='"+
                user.getPassword() + "'where id="+ user.getId();
        DBUtil.executeUpdate(sql);
        return 0;
    }

    @Override
    public List<User> queryByCondition(Query queryCondition) throws SQLException {
        String sql = "select * from t_user where 1=1 ";
        if (queryCondition.getId()!=null){
            sql+="&&id="+queryCondition.getId();
        }
        if (queryCondition.getName()!=null){
            sql+="&&name='"+queryCondition.getName()+"'";
        }
        if (queryCondition.getCode()!=null){
            sql+="&&code='"+queryCondition.getCode()+"'";
        }
        List<User> list = new ArrayList<User>();
        User user = null;
        //遍历集合
        ResultSet rs = DBUtil.executeQuery(sql);
        while (rs.next()) {
            int id = rs.getInt("id");
            String name = rs.getString("name");
            String password = rs.getString("password");
            String code = rs.getString("code");

            user = new User();
            user.setId(id);
            user.setName(name);
            user.setPassword(password);
            user.setCode(code);
            //装载集合
            list.add(user);
        }
        return list;
    }

}
