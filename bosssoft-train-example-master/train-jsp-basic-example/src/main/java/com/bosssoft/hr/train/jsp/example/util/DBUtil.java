package com.bosssoft.hr.train.jsp.example.util;

import com.sun.rowset.CachedRowSetImpl;
import lombok.extern.slf4j.Slf4j;

import java.sql.*;


/**
 * @description: 我是工具类并且我不喜欢被继承 final 保护了我免于继承，private 保护我被创建
 * @author: Administrator
 * @create: 2020-05-28 20:45
 * @since
 **/
@Slf4j
public final class DBUtil {
    private static final String IP = "127.0.0.1";
    private static final int PORT = 3306;
    private static final String DATABASE = "learn";
    private static final String LOGIN_NAME = "root";
    private static final String PASSWORD = "123456";
    private static final String DB_URL = "jdbc:mysql://localhost/learn?serverTimezone=Asia/Shanghai";

    private DBUtil() {
    }

    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {

        }
    }


    public static Connection createConnection() throws SQLException {
        return DriverManager.getConnection(DB_URL, LOGIN_NAME, PASSWORD);
    }


    public static ResultSet executeQuery(final String sql) throws SQLException {
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            CachedRowSetImpl rowSet = new CachedRowSetImpl();
            rowSet.populate(statement.executeQuery(sql));
            return rowSet;
        }
    }
    public static int executeUpdate(final String sql, final Object... arg)
            throws SQLException {
        try (Connection connection = createConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            return statement.executeUpdate();
        }
    }
}
