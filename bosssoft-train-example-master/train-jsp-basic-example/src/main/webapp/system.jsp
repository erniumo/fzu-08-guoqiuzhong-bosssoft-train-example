<%@ page import="java.awt.*" %>
<%@ page import="com.bosssoft.hr.train.jsp.example.pojo.User" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2020/7/13
  Time: 15:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%--自定义标签--%>
<%@taglib uri="/userTag" prefix="userTag" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>System</title>
</head>
<body>
    <table>
        <caption>用户列表</caption>
        <thead>
        <tr>
            <th>ID</th><th>Name</th><th>Code</th><th>Password</th><th>操作</th>
        </tr>
        </thead>
        <tbody>
       <%-- <c:forEach items="$users" var="user">
            <tr>
                <td>${user.id}</td>
                <td>${user.getName()}</td>
                <td>${user.getCode()}</td>
                <td>${user.getPassword()}</td>
                <td>
                    <a href="/train_jsp_basic_example_war/updateUser?flag=0&id="+${user.getId()}>修改</a>
                    <a href="/train_jsp_basic_example_war/deleteUser?id="+${user.getId()}>删除</a>
                </td>
            </tr>
        </c:forEach>--%>
       <%
           List<User> users = (List) request.getAttribute("users");
           for(User user:users){
       %>
           <tr>
               <td><%=user.getId()%></td>
               <td><%=user.getName()%></td>
               <td><%=user.getCode()%></td>
               <td><%=user.getPassword()%></td>
               <td>
                   <a href="/train_jsp_basic_example_war/updateUser?flag1=0&id=<%=user.getId()%>">修改</a>
                   <a href="/train_jsp_basic_example_war/deleteUser?id=<%=user.getId()%>">删除</a>
               </td>
           </tr>
       <%
           }
       %>
        </tbody>
    </table>
<a href="addUser.jsp">添加用户</a>
<form action="/train_jsp_basic_example_war/findUsers" method="post">
    <input id="flag" name="flag" type="hidden" value="1">
    <input id="name" name="name" type="text">
    <input type="submit" value="搜索">
</form>
</body>
</html>