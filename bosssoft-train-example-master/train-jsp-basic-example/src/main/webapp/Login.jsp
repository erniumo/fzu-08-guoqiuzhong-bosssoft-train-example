<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2020/7/12
  Time: 15:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<form id="loginform"  name="loginform" action="" method="POST">
    账号:<input id="username" name="username" type="text" >
    密码:<input id="password" name="password" type="password">
    <input type="submit" value="登录" id="btn" onclick="login(this.form)">
</form>
<script src="/train_jsp_basic_example_war/sourse/js/jquery-3.4.1.min.js"></script>
<script language="javascript">
    function login(loginform){//传入表单参数
        if(loginform.username.value==""){       //验证用户名是否为空
            alert("请输入用户名！");loginform.username.focus();return false;
        }
        if(loginform.password.value==""){       //验证密码是否为空
            alert("请输入密码！");loginform.password.focus();return false;
        }
        $.ajax({
            url:"/train_jsp_basic_example_war/login",
            type:"POST",
            data:{username:loginform.username.value,password:loginform.password.value},
            dataType:"json",
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',//防止乱码
            success:function(data){
                if(data.code == 1){
                    alert("您输入的用户名或密码有错！");loginform.username.focus();return false;
                }else{
                    alert("ok");
                    window.location.href = "/train_jsp_basic_example_war/findUsers?flag=0" ;//跳转到主页
                }
            }
        });
    }
</script>
</body>
</html>
