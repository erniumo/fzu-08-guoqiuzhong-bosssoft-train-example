<%@ page import="com.bosssoft.hr.train.jsp.example.pojo.User" %><%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2020/7/14
  Time: 18:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>修改</title>
</head>
<body>
<%
    User user = (User)request.getAttribute("user");
%>
<form id="updateform"  name="updateform" action="/train_jsp_basic_example_war/updateUser" method="POST">
    <input id="id" name="id" type="hidden" value=<%= user.getId()%>>
    <input id="flag" name="flag1" type="hidden" value="1">
    账号:<input id="code" name="code" type="text" value=<%= user.getCode()%>>
    密码:<input id="password" name="password" type="password" value=<%= user.getPassword()%>>
    姓名:<input id="username" name="username" type="text" value=<%= user.getName()%>>
    <input type="submit" value="修改" id="btn" >
</form>
</body>
</html>
