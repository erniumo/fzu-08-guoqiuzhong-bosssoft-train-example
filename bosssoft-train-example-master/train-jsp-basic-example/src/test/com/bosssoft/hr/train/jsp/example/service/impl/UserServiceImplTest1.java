package com.bosssoft.hr.train.jsp.example.service.impl;


import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.service.UserService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;

public class UserServiceImplTest1 {

    private UserService userService=new UserServiceImpl();
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void save() {
        User user =new User(10,"jim");
        Assert.assertEquals(true,userService.save(user));
        //测试没有id 和 名字的参数
        User user2 =new User();
        Assert.assertEquals(true,userService.save(user));

        // 如果需要测试为空等继续构建 可以验证的 User
    }

    /**
     * 测试插入重复
     */
    @Test
    public void saveSameId() {
        User user =new User(1,"jim");
        Assert.assertEquals(false,userService.save(user));
    }


    @Test
    public void remove() throws SQLException {
        User user =new User(1,"jim");
        Assert.assertEquals(true,userService.remove(user));
    }

    /**
     * 依据界面可能开展的测试编写测试用例  例如某一些字段为空等，实际就是依据
     * 项目需求的测试用例来扩展测试用例
     */
    @Test
    public void update() throws SQLException {
        User user =new User(10,"jims");
        Assert.assertEquals(false,userService.update(user));
    }

    @Test
    public void queryByCondition() throws SQLException {
        Query q = new Query();
        q.setName("jims");
        List<User> users = userService.queryByCondition(q);
        if (users==null){
            System.out.println("null");
        }else {
            System.out.println(users.get(0).getName());
        }

    }

    /**
     * 测试正确账号和错误账号
     */
    @Test
    public void authentication() throws SQLException {
        //测试正确
        User user=new User("001","123456");
        Assert.assertEquals(true, userService.authentication(user));

        //测试错误账号
        User user2=new User("003","123456");
        Assert.assertEquals(false, userService.authentication(user2));

    }
}
